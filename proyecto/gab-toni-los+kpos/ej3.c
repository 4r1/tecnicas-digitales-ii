#include <termios.h>
#include <stdio.h>
#include <string.h>

#define LONGITUD 5

static struct termios old, new;
char ch[LONGITUD+1];

/* Initialize new terminal i/o settings */
void initTermios(int echo)
{
  tcgetattr(0, &old);           /* toma la configuración antigua de i/o de la terminal (grab old terminal i/o settings) */
  new = old;                    /* genera una nueva estructura de configuración igual a la antigua (make new settings same as old settings) */
  new.c_lflag &= ~ICANON;       /* disable buffered i/o */
  if (echo == 2) {
      new.c_lflag |= ECHO;      /* setea en ON el modo ECO (set echo mode) */
  } else {
      new.c_lflag &= ~ECHO;     /* setea en Off el modo ECO (set no echo mode) */
  }
  tcsetattr(0, TCSANOW, &new);  /* setea la terminal con los nuevos atributos (use these new terminal i/o settings now) */
}

/* Resetea la terminal a la configuración antigua (Restore old terminal i/o settings) */
void resetTermios(void)
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char *getch_(int echo)
{
  int i=0;
  int caracter;

  initTermios(echo);           /* inicializa la terminal con el modo de ECO pasado como parametro */

  while(i<LONGITUD)
  {
    caracter = getchar();
    if(caracter == '\033')
    {
      getchar();
      getchar();
      continue;
    }

    if((caracter == 127) && (i > 0)) // Si es el caracter borrar, borra
    {
      i--;
    }

    else if((caracter >= 32) && (caracter <= 126)) // Solo acepta los caracteres imprimibles
    {
      ch[i] = caracter;
      i++;
    }
  }

  ch[LONGITUD+1] = '\0';
  resetTermios();               /* restaura la terminal a su configuracion previa a la ejecucion del programa */
  return ch;
}

/* Read 1 character without echo */
char *getch(void)
{
  return getch_(0);
}

/* Let's test it out */
int main(void) {
  int i;
  char clave[] = "tonai";
  char *cadena;

  for(i=0; i<3; i++)
  {
    printf("Ingresar clave: ");
    cadena = getch();
    if(strcmp(clave, cadena) == 0)
    {
      printf("\nBienvenido al sistema!\n");
      break;
    }

    else
    {
      printf("\nClave incorrecta!\n");
      if(i==2)
      {
        return 0;
      }
    }
  }

  printf("\nEntreishon\n\n");

  return 0;
}
