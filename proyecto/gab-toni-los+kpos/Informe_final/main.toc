\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Enunciado}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}WiringPi y EasyPIO.h}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}WiringPi}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}EasyPIO.h}{2}{section.2.2}
\contentsline {subsection}{Introducci\IeC {\'o}n}{2}{section*.2}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{3}{section*.3}
\contentsline {chapter}{\numberline {3}Desarrollo}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Ejercicio 1 - Estructura Switch-Case en assembly}{7}{section.3.1}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{7}{section*.4}
\contentsline {subsection}{Funcionamiento}{8}{section*.5}
\contentsline {section}{\numberline {3.2}Ejercicio 2 - Retardo en assembly}{8}{section.3.2}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{8}{section*.6}
\contentsline {subsection}{Funcionamiento}{9}{section*.7}
\contentsline {section}{\numberline {3.3}Ejercicio 3 - Control de acceso al sistema mediante password en c}{9}{section.3.3}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{9}{section*.8}
\contentsline {subsection}{Funcionamiento}{10}{section*.9}
\contentsline {section}{\numberline {3.4}Ejercicio 4 - Bucle repetidor de entradas}{11}{section.3.4}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{11}{section*.10}
\contentsline {subsection}{Funcionamiento}{11}{section*.11}
\contentsline {section}{\numberline {3.5}Ejercicio 5 - Auto fant\IeC {\'a}stico}{11}{section.3.5}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{11}{section*.12}
\contentsline {subsection}{Funcionamiento}{12}{section*.13}
\contentsline {section}{\numberline {3.6}Ejercicio 6 - La carrera}{12}{section.3.6}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{12}{section*.14}
\contentsline {subsection}{Funcionamiento}{13}{section*.15}
\contentsline {section}{\numberline {3.7}Ejercicio 7 - ADC con $I^2C$}{13}{section.3.7}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{13}{section*.16}
\contentsline {subsection}{Funcionamiento}{14}{section*.17}
\contentsline {section}{\numberline {3.8}Ejercicio 8 - Comunicaci\IeC {\'o}n serial}{14}{section.3.8}
\contentsline {subsection}{C\IeC {\'o}digo fuente}{14}{section*.18}
\contentsline {subsection}{Funcionamiento}{15}{section*.19}
