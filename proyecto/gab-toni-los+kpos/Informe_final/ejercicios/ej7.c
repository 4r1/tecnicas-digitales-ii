#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>

const char direccion = 0x48; /*direccion i2c*/

int main()   {

int fd, valoradc;

if (wiringPiSetup () == -1) exit (1);	

fd = wiringPiI2CSetup(direccion);

while (1)   {
	wiringPiI2CReadReg8(fd, direccion + 0); 
	valoradc = wiringPiI2CReadReg8(fd, direccion + 0);
	printf("Valor potenciometro= %d \n", valoradc);
	
	wiringPiI2CReadReg8(fd, direccion + 1) ; 
	valoradc = wiringPiI2CReadReg8(fd, direccion + 1) ;
	printf("Fotocelula = %d \n", valoradc);
	
	wiringPiI2CReadReg8(fd, direccion + 2) ; 
	valoradc = wiringPiI2CReadReg8(fd, direccion + 2) ;
	printf("Termistor = %d \n\n", valoradc);
			  
	delay(500);
     }
			  
return 0;
			  
}
