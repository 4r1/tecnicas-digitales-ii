#include <stdio.h>
#include "EasyPIO.h"

int main () {
	pioInit();

	pinMode(21, OUTPUT);

	noInterrupts();

	for(int i=0; i<20; i++)
	{
		digitalWrite(21,1);
		delayMillis(250);
		digitalWrite(21,0);
		delayMillis(250);
		printf("\nPuto el que lee");
	}

	printf("\n");

	interrupts();

	return 0;
}

void delayMicros(int micros)
{
	SYS_TIMER_C1 = SYS_TIMER_CLO + micros;
	SYS_TIMER_CSbits.M1 = 1;
	while(SYS_TIMER_CSbits.M1 == 0);
}

void delayMillis(int millis)
{
	delayMicros(millis*1000);
}
