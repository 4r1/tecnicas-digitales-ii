.arch armv6
.global main

;Implementa un menu que arranca con 3 numeros (registros R1, R2 y R3) y en base a la opcion elegida 
;hace una determinada comparacion. Si es verdadera devuelve un 1 en R0, si no, un 0

main:
	MOV R1, #10 ;Los tres números que se van a comparar
	MOV R2, #15
	MOV R3, #20

	MOV R4, #2 ;Opcion

	CMP R4, #1
	BLEQ COMP1

	CMP R4, #2
	BLEQ COMP2
	
	CMP R4, #3
	BLEQ COMP3

	CMP R4, #4
	BLEQ COMP4
	
	B END

COMP1:	MOV R0, #1 ; Si R1 es menor a 100 y es mayor a 20, devuelve un 1 en R0
	CMP R1, #100
	MOVGE R0, #0
	BGE END1
	CMP R1, #20
	MOVLE R0, #0
END1:	MOV PC, LR

COMP2:	MOV R0, #0 ; Si R1 es menor a 100 o R2 es mayor a 20, devuelve un 1 en R0
	CMP R1, #100
	MOVLT R0, #1
	BLT END2
	CMP R2, #20
	MOVGT R0, #1
END2:	MOV PC, LR

COMP3:	MOV R0, #0 ; Si R1 es igual a 10 o igual a 15 o igual a 20, devuelve un 1 en R0
	CMP R1, #10
	MOVEQ R0, #1
	BEQ END3
	CMP R1, #15
	MOVEQ R0, #1
	BEQ END3
	CMP R1, #20
	MOVEQ R0, #1
END3:	MOV PC, LR

COMP4:	MOV R0, #1 ; Si R1 es igual a 10 y R2 es igual a 15 y R3 es igual a 20, devuelve un 1 en R0
	CMP R1, #10
	MOVNE R0, #0
	BNE END4
	CMP R2, #15
	MOVNE R0, #0
	BNE END4
	CMP R3, #20
	MOVNE R0, #0
END4:	MOV PC, LR

	MOV R0, #0
END: 
