#include "EasyPIO.h"
#include <wiringPi.h>
#include <stdio.h>

int main () {
	pioInit();

	for(int i=0;i<8;i++)
		pinMode( outLED[i], OUTPUT);

	pinMode( inSW1, INPUT);

	while ( !digitalRead(inSW1) ) {
		for(int i=0;i<8 && !digitalRead(inSW1);i++) {
			delay(500);	

			if(i!=0)
				digitalWrite( outLED[i-1], 0);
			else
				digitalWrite( outLED[i+1], 0);

			digitalWrite( outLED[i], 1);
		}
		
		for(int i=6;i>0 && !digitalRead(inSW1);i--) {
			delay(500);

			digitalWrite( outLED[i+1],0);
			digitalWrite( outLED[i],1);
		}
	}

	return 0;
}
