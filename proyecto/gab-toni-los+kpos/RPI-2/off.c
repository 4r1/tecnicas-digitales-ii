#include <stdio.h>
#include "EasyPIO.h"

#define PIN 23

int main () {
	pioInit();

	pinMode(PIN, OUTPUT);

	digitalWrite(PIN, 1);
	return 0;
}
