#include "EasyPIO.h"

int main(void)
{
	pioInit();

	pinMode(5,INPUT);
	pinMode(23,OUTPUT);

	while(1)
	{	
		if(digitalRead(5) == 1)
		{
			digitalWrite(23, 0);
		}

		else
		{
			digitalWrite(23, 1);
		}
	}
	
	return 0;
}
