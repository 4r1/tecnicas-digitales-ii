#ifndef easyPIOlib
#define easyPIOlib

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#define BCM2837_PERI_BASE 	0x3F000000
#define GPIO_BASE 			(BCM2837_PERI_BASE + 0x200000)
volatile unsigned int *gpio;
#define GPFSEL ((volatile unsigned int *) (gpio + 0))
#define GPSET ((volatile unsigned int *) (gpio + 7))
#define GPCLR ((volatile unsigned int *) (gpio + 10))
#define GPLEV ((volatile unsigned int *) (gpio + 13))
#define GPLEV0 				(* (volatile unsigned int *) (gpio + 13))
#define BLOCK_SIZE			(4*1024)
#define INPUT 0
#define OUTPUT 1

void pioInit(void);
void pinMode(int pin, int funcion);
void digitalWrite(int pin, int val);
int digitalRead(int pin);

#endif
