#ifndef easyPIOlib
#define easyPIOlib

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

//GPIO preproc
#define BCM2837_PERI_BASE 	0x3F000000
#define GPIO_BASE 			(BCM2837_PERI_BASE + 0x200000)
volatile unsigned int *gpio;
#define GPFSEL ((volatile unsigned int *) (gpio + 0))
#define GPSET ((volatile unsigned int *) (gpio + 7))
#define GPCLR ((volatile unsigned int *) (gpio + 10))
#define GPLEV ((volatile unsigned int *) (gpio + 13))
#define GPLEV0 				(* (volatile unsigned int *) (gpio + 13))
#define BLOCK_SIZE			(4*1024)
#define INPUT 0
#define OUTPUT 1

typedef struct {
	unsigned M0	:1;
	unsigned M1	:1;
	unsigned M2	:1;
	unsigned M3	:1;
	unsigned 	:28;
} sys_timer_csbits;

//Timer y interrupts preproc
#define SYS_TIMER_BASE	(BCM2837_PERI_BASE + 0x3000)
volatile unsigned int *sys_timer;
#define SYS_TIMER_CS ((volatile unsigned int *) (sys_timer + 0))
#define SYS_TIMER_CSbits ((volatile sys_timer_csbits*) (sys_timer + 0))
#define SYS_TIMER_CLO ((volatile unsigned int *) (sys_timer + 4))
#define SYS_TIMER_CHI ((volatile unsigned int *) (sys_timer + 8))
#define SYS_TIMER_C0 ((volatile unsigned int *) (sys_timer + 12))
#define SYS_TIMER_C1 ((volatile unsigned int *) (sys_timer + 16))
#define SYS_TIMER_C2 ((volatile unsigned int *) (sys_timer + 20))
#define SYS_TIMER_C3 ((volatile unsigned int *) (sys_timer + 24))

#define ARM_TIMER_BASE (BCM2837_PERI_BASE + 0xB000)
volatile unsigned int *arm_timer;
#define IRQ_PENDING_BASIC ((volatile unsigned int *) (arm_timer + 128))
#define IRQ_PENDING1 ((volatile unsigned int *) (arm_timer + 129))
#define IRQ_PENDING2 ((volatile unsigned int *) (arm_timer + 130))

#define IRQ_ENABLE1 ((volatile unsigned int *) (arm_timer + 132))
#define IRQ_ENABLE2 ((volatile unsigned int *) (arm_timer + 133))
#define IRQ_ENABLE_BASIC ((volatile unsigned int *) (arm_timer + 134))
#define IRQ_DISABLE1 ((volatile unsigned int *) (arm_timer + 135))
#define IRQ_DISABLE2 ((volatile unsigned int *) (arm_timer + 136))
#define IRQ_DISABLE_BASIC ((volatile unsigned int *) (arm_timer + 137))

int irq1, irq2, irqbasic;

int inDIP[4] = {5,6,13,19};
int inSW1 = 17;
int inSW2 = 27;
int outLED[8] = {23,24,25,12,16,20,21,26};

void pioInit(void);
void pinMode(int, int);
void digitalWrite(int, int);
int digitalRead(int);
void delayMicros (int micros);
void delayMillis (int millis);
void noInterrupts (void);
void interrupts (void);

#endif

void pioInit () {
	int mem_fd;
	void *reg_map;
	mem_fd = open("/dev/mem", O_RDWR|O_SYNC); // abre /dev/mem
	reg_map = mmap(
		NULL,					// dir. donde comienza el mapeo (null=no importa)
		BLOCK_SIZE,				// 4KB bloque mapeado
		PROT_READ|PROT_WRITE, 	// permite lect.y escr.en la mem.
		MAP_SHARED,				// acceso no exclusivo
		mem_fd,					// puntero a /dev/mem
		GPIO_BASE);				// offset a la GPIO
	gpio = (volatile unsigned *)reg_map;

	reg_map = mmap (
		NULL,
		BLOCK_SIZE,
		PROT_READ|PROT_WRITE,
		MAP_SHARED,
		mem_fd,
		SYS_TIMER_BASE);

	sys_timer=(volatile unsigned *)reg_map;

	reg_map = mmap (
		NULL,
		BLOCK_SIZE,
		PROT_READ|PROT_WRITE,
		MAP_SHARED,
		mem_fd,
		ARM_TIMER_BASE);

	arm_timer=(volatile unsigned *)reg_map;

	close(mem_fd);	
}

void pinMode(int pin, int funcion) {
	int reg = pin/10;
	int offset = (pin%10) *3;

	GPFSEL[reg] &= ~((0b111 & ~funcion) << offset);
	GPFSEL[reg] |= ((0b111 & funcion) << offset);
}

void digitalWrite(int pin, int val) {
	int reg = pin / 32;
	int offset = pin % 32;

	if (val)
		GPSET[reg] = 1 << offset;
	else
		GPCLR[reg] = 1 << offset;
}

int digitalRead(int pin) {
	int reg = pin / 32;
	int offset = pin % 32;

	return (GPLEV[reg] >> offset) & 0x00000001;
}

void delayMicros (int micros) {
	*SYS_TIMER_C1 = *SYS_TIMER_CLO + micros;
	SYS_TIMER_CSbits->M1 = 1;
	while (SYS_TIMER_CSbits->M1 == 0);
}

void delayMillis (int millis) {
	delayMicros (millis*1000);
}

void noInterrupts(void) {
    //save current interrupts
    irq1 = *IRQ_ENABLE1;
    irq2 = *IRQ_ENABLE2;
    irqbasic = *IRQ_ENABLE_BASIC;

    //disable interrupts
    *IRQ_DISABLE1 = irq1;
    *IRQ_DISABLE2 = irq2;
    *IRQ_DISABLE_BASIC = irqbasic; 
}

void interrupts(void) {
    if(*IRQ_ENABLE1 == 0){ // if interrupts are disabled
    //restore interrupts
        *IRQ_ENABLE1 = irq1;
        *IRQ_ENABLE2 = irq2;
        *IRQ_ENABLE_BASIC = irqbasic;
    }
}
