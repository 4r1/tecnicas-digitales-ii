#include "EasyPIO.h"
#include <stdio.h>

int main () {
	pioInit();

	for(int i=0;i<8;i++)
		pinMode( outLED[i], OUTPUT);

	pinMode( inSW1, INPUT);

	while ( digitalRead(inSW1) ) {
		for(int i=0;i<8;i++) {
			delayMillis(200);

			if(i!=0)
				digitalWrite( outLED[i-1], 0);

			digitalWrite( outLED[i], 1);
		}
		
		for(int i=7;i>=0;i--) {
			delayMillis(200);

			if(i!=7)
				digitalWrite( outLED[i+1], 0);

			digitalWrite( outLED[i],1);
		}
	}

	return 0;
}
