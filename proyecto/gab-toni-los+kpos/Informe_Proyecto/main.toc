\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Enunciado}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Condiciones de presentaci\IeC {\'o}n}{2}{section.1.2}
\contentsline {chapter}{\numberline {2}C\IeC {\'o}digo fuente}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}Funcionamiento}{22}{chapter.3}
\contentsline {section}{\numberline {3.1}Funciones principales}{22}{section.3.1}
\contentsline {subsection}{int main (void);}{22}{section*.2}
\contentsline {subsection}{char* getch(void);}{22}{section*.3}
\contentsline {subsection}{void initTermios (void);}{22}{section*.4}
\contentsline {subsection}{void resetTermios(void);}{22}{section*.5}
\contentsline {subsection}{short int menu (short int);}{23}{section*.6}
\contentsline {section}{\numberline {3.2}Funciones de secuencias de luces}{23}{section.3.2}
\contentsline {subsection}{El auto fant\IeC {\'a}stico (autofantastico)}{23}{section*.8}
\contentsline {subsection}{El choque (choque)}{23}{section*.9}
\contentsline {subsection}{La apilada (apilada)}{23}{section*.10}
\contentsline {subsection}{La carrera (carrera)}{24}{section*.11}
\contentsline {subsection}{Azar (azar)}{24}{section*.12}
\contentsline {subsection}{Anulaci\IeC {\'o}n (anulacion)}{24}{section*.13}
\contentsline {subsection}{ZigZag (zigzag)}{24}{section*.14}
\contentsline {subsection}{Barras (barras)}{24}{section*.15}
\contentsline {section}{\numberline {3.3}Funciones auxiliares utilizadas}{24}{section.3.3}
\contentsline {subsection}{char* titulo (int);}{24}{section*.16}
\contentsline {subsection}{int kbhit(void);}{24}{section*.17}
\contentsline {subsection}{int delayInter(int, shor int);}{24}{section*.18}
