#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include "EasyPIO.h"
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <wiringSerial.h>

#define LONGITUD 5

static struct termios old, new;
char ch[LONGITUD+1];

//--------i2c-------------------------------------
const char PCF8591 = 0x48; //Direccion potenciometro i2c
//------------------------------------------------

int puerto;

char *titulo(int secuencia)
{
  switch(secuencia)
  {
    case 1:
    return "\nEjecutando secuencia \"El auto fantastico\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 2:
    return "\nEjecutando secuencia \"El choque\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 3:
    return "\nEjecutando secuencia \"La apilada\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 4:
    return "\nEjecutando secuencia \"La carrera\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 5:
    return "\nEjecutando secuencia \"Azar\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 6:
    return "\nEjecutando secuencia \"Anulacion\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 7:
    return "\nEjecutando secuencia \"Zigzag\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;

    case 8:
    return "\nEjecutando secuencia \"Barras\" \n\nPrecione la tecla \"q\" para detener\n\n";
    break;
  }
}

int kbhit()
{
    static struct termios term, term2;
    tcgetattr(0, &term);

    term2 = term;
    term2.c_lflag &= ~ICANON;
    tcsetattr(0, TCSANOW, &term2);

    int byteswaiting;
    ioctl(0, FIONREAD, &byteswaiting);

    tcsetattr(0, TCSANOW, &term);

    return byteswaiting > 0;
}

void initTermios(void)
{
  tcgetattr(0, &old);           /* toma la configuración antigua de i/o de la terminal (grab old terminal i/o settings) */
  new = old;                    /* genera una nueva estructura de configuración igual a la antigua (make new settings same as old settings) */
  new.c_lflag &= ~ICANON;
  new.c_lflag &= ~ECHO;     /* setea en Off el modo ECO (set no echo mode) */
  tcsetattr(0, TCSANOW, &new);  /* setea la terminal con los nuevos atributos (use these new terminal i/o settings now) */
}

/* Resetea la terminal a la configuración antigua (Restore old terminal i/o settings) */
void resetTermios(void)
{
  tcsetattr(0, TCSANOW, &old);
}

char *getch(void)
{
  int i=0;
  int caracter;

  initTermios();           /* inicializa la terminal con el modo de ECO pasado como parametro */

  while(i<LONGITUD)
  {
    caracter = getchar();
    if(caracter == '\033')
    {
      getchar();
      getchar();
      continue;
    }

    if((caracter == 127) && (i > 0)) // Si es el caracter borrar, borra
    {
      i--;
    }

    else if((caracter >= 32) && (caracter <= 126)) // Solo acepta los caracteres imprimibles
    {
      ch[i] = caracter;
      i++;
    }
  }

  ch[LONGITUD+1] = '\0';
  resetTermios();               /* restaura la terminal a su configuracion previa a la ejecucion del programa */
  return ch;
}

//------Delay interrumpible-----

int delayInter(int secuencia, short int ejecucion)
{
  int pote, fd;
  static int delays[2][8] = {{500, 500, 500, 500, 500, 500, 500, 500},{0, 0, 0, 0, 0, 0, 0, 0}}; // en ms; 0=autoFantastico, 1=choque, 2=apilada, 3=carrera, 5=random
  char caracter;

  if((fd = wiringPiI2CSetup(PCF8591)) == -1)
  {
    system("clear");
    printf("\nERROR EN LA CONEXION I2C");
    return 1;
  }

  if(delays[1][secuencia] == 0)
  {
    wiringPiI2CReadReg8(fd, PCF8591); // Se debe obtener dos veces
    pote = ((wiringPiI2CReadReg8(fd, PCF8591)*1975)/255)+25; // Conversion de lectura a ms
    delays[0][secuencia] = pote;
    delays[1][secuencia] = 1;
  }

  initTermios();

  for(int i=0; i<delays[0][secuencia]; i++) // Hace un for de 0 al valor del delay establecido
  {
    delay(1);

    if((kbhit()==1) && (ejecucion==0)) // Si se detecta la pulsacion de una tecla y se esta ejecutando localmente
    {
      caracter = getchar();
      if((caracter == 'q') || (caracter == 'Q'))
      {
        system("clear");
        for(int j=0; j<8; j++)
          digitalWrite(outLED[j], 0); // Apaga todos los leds al salir
        resetTermios();
        return 1;
      }

      else if(caracter == '\033')
      {
        getchar();

        switch(getchar())
        {
          case 'A':
          if(delays[0][secuencia] > 25)
          {
            delays[0][secuencia] = delays[0][secuencia] - 25;
            system("echo VELOCIDAD +"); //Se utiliza echo porque printf retrasa la salida del elemento en el buffer
          }
          break;

          case 'B':
          if(delays[0][secuencia] < 2000)
          {
            delays[0][secuencia] = delays[0][secuencia] + 25;
            system("echo VELOCIDAD -");
          }
          break;
        }
      }

      else
      {
        continue;
      }
    }

    else if((serialDataAvail(puerto)) && (ejecucion==2))
    {
      caracter = serialGetchar(puerto);

      if((caracter == 'q') || (caracter == 'Q'))
      {
        system("clear");
        for(int j=0; j<8; j++)
          digitalWrite(outLED[j], 0); // Apaga todos los leds al salir
        resetTermios();
        return 1;
      }

      else if(caracter == '\033')
      {
        serialGetchar(puerto);

        switch(serialGetchar(puerto))
        {
          case 'A':
          if(delays[0][secuencia] > 25)
          {
            delays[0][secuencia] = delays[0][secuencia] - 25;
          }
          break;

          case 'B':
          if(delays[0][secuencia] < 2000)
          {
            delays[0][secuencia] = delays[0][secuencia] + 25;
          }
          break;
        }
      }
    }
  }

  resetTermios();

  return 0;
}

//------------------------------

//-------Secuencias de LED------
void autoFantastico(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(1));
	while(1) {
		for(int i=0;i<8;i++) {
			if(delayInter(0, ejecucion))
      {
        return;
      }

			if(i!=0)
				digitalWrite(outLED[i-1], 0);
			else
				digitalWrite(outLED[i+1], 0);

			digitalWrite(outLED[i], 1);
		}

		for(int i=6;i>0;i--) {
      if(delayInter(0, ejecucion))
      {
        return;
      }

			digitalWrite( outLED[i+1],0);
			digitalWrite( outLED[i],1);
		}
	}
}


void choque(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(2));

  while(1)
  {
    for(int i=0; i<4; i++)
    {
      digitalWrite(outLED[i], 1);
      digitalWrite(outLED[7-i], 1);

      if(i!=0)
      {
        digitalWrite(outLED[i-1], 0);
        digitalWrite(outLED[8-i], 0);
      }

      if(delayInter(1, ejecucion))
      {
        return;
      }
    }

    for(int i=3; i>=0; i--)
    {
      digitalWrite(outLED[i], 0);
      digitalWrite(outLED[7-i], 0);

      if(i!=0)
      {
        digitalWrite(outLED[i-1], 1);
        digitalWrite(outLED[8-i], 1);
      }

      if(delayInter(1, ejecucion))
      {
        return;
      }
    }
  }
}


void apilada(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(3));

  while(1)
  {
    for(int i=0; i<8; i++)
      digitalWrite(outLED[i], 0);

    for(int i=0; i<8; i++)
    {
      for(int j=0; j<8-i; j++)
      {
        digitalWrite(outLED[j], 1);

        if(j!=0)
        {
          digitalWrite(outLED[j-1], 0);
        }

        if(delayInter(2, ejecucion))
        {
          return;
        }

        if(j==(7-i))
        {
          digitalWrite(outLED[j], 0);

          if(delayInter(2, ejecucion))
          {
            return;
          }

          digitalWrite(outLED[j], 1);

          if(delayInter(2, ejecucion))
          {
            return;
          }
        }
      }
    }
  }
}


void carrera(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(4));

  static const unsigned int lut[18] =	// La LUT se implementa con static y const para permitir optimizaciones
	{
		// Valores calculados manualmente, es simplemente pasar del binario de 8 leds al hexa correspondiente
		0x00U, 0x00U,
		0x01U, 0x01U,
		0x02U, 0x02U,
		0x04U, 0x04U,
		0x08U, 0x08U,
		0x11U, 0x12U,
		0x24U, 0x28U,
		0x50U, 0x60U,
		0xC0U, 0x80U,
	};

  while(1)
  {	// Mientras el usuario no decidio finalizar secuencia
    for(int i=0; i<18; i++)
    {	// Recorro los 18 valores de mi LUT
    	(lut[i] & 0x01) ? digitalWrite(outLED[0], HIGH) : digitalWrite(outLED[0], LOW);
    	(lut[i] & 0x02) ? digitalWrite(outLED[1], HIGH) : digitalWrite(outLED[1], LOW);
  		(lut[i] & 0x04) ? digitalWrite(outLED[2], HIGH) : digitalWrite(outLED[2], LOW);
  		(lut[i] & 0x08) ? digitalWrite(outLED[3], HIGH) : digitalWrite(outLED[3], LOW);
  		(lut[i] & 0x10) ? digitalWrite(outLED[4], HIGH) : digitalWrite(outLED[4], LOW);
  		(lut[i] & 0x20) ? digitalWrite(outLED[5], HIGH) : digitalWrite(outLED[5], LOW);
  		(lut[i] & 0x40) ? digitalWrite(outLED[6], HIGH) : digitalWrite(outLED[6], LOW);
  		(lut[i] & 0x80) ? digitalWrite(outLED[7], HIGH) : digitalWrite(outLED[7], LOW);
      if(delayInter(4, ejecucion))
      {
        return;
      }
    }
  }
}


void azar(short int ejecucion)
{
  int flag = 1;
  int leds[8] = {0, 1, 2, 3, 4, 5, 6, 7};
  int temp, random, suma=0;

  system("clear");
  printf("%s", titulo(5));

  while(1)
  {
    //--------Mezcla al azar leds---------
    for(int i = 0; i < 20; i++)
    {
      for(int j = 0; j < 8; j++)
      {
        random = rand()%3;
        suma = j+random;
        if(suma>7)
          suma = suma - 7;
        temp = leds[suma];
        leds[suma] = leds[j];
        leds[j] = temp;
      }
    }
    //------------------------------------

    for(int i = 0; i < 8; i++)
    {
      digitalWrite(outLED[leds[i]], 1);

      if(delayInter(3, ejecucion))
      {
        return;
      }
    }

    for(int i = 0; i < 4; i++)
    {
      digitalWrite(outLED[i], 0);
      digitalWrite(outLED[7-i], 0);
      if(delayInter(3, ejecucion))
      {
        return;
      }
    }
  }
}

void anulacion(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(6));

  static const unsigned int lut[15] =	// La LUT se implementa con static y const para permitir optimizaciones
	{
		// Valores calculados manualmente, es simplemente pasar del binario de 8 leds al hexa correspondiente
		0x00U, 0x81U,
		0xC3U, 0xE7U,
		0xFFU, 0xFFU,
		0xFFU, 0x7EU,
		0x3CU, 0x18U,
		0x00U, 0x18U,
		0x00U, 0x18U,
		0x00U
	};

  while(1)
  {	// Mientras el usuario no decidio finalizar secuencia
    for(int i=0; i<15; i++)
    {	// Recorro los 18 valores de mi LUT
    	(lut[i] & 0x01) ? digitalWrite(outLED[0], HIGH) : digitalWrite(outLED[0], LOW);
    	(lut[i] & 0x02) ? digitalWrite(outLED[1], HIGH) : digitalWrite(outLED[1], LOW);
  		(lut[i] & 0x04) ? digitalWrite(outLED[2], HIGH) : digitalWrite(outLED[2], LOW);
  		(lut[i] & 0x08) ? digitalWrite(outLED[3], HIGH) : digitalWrite(outLED[3], LOW);
  		(lut[i] & 0x10) ? digitalWrite(outLED[4], HIGH) : digitalWrite(outLED[4], LOW);
  		(lut[i] & 0x20) ? digitalWrite(outLED[5], HIGH) : digitalWrite(outLED[5], LOW);
  		(lut[i] & 0x40) ? digitalWrite(outLED[6], HIGH) : digitalWrite(outLED[6], LOW);
  		(lut[i] & 0x80) ? digitalWrite(outLED[7], HIGH) : digitalWrite(outLED[7], LOW);
      if(delayInter(5, ejecucion))
      {
        return;
      }
    }
  }
}

void zigzag(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(7));

  static const unsigned int lut[13] =	// La LUT se implementa con static y const para permitir optimizaciones
	{
		// Valores calculados manualmente, es simplemente pasar del binario de 8 leds al hexa correspondiente
		0x00U, 0x10U,
		0x08U, 0x20U,
		0x04U, 0x40U,
		0x02U, 0x80U,
		0x01U, 0x00U,
    0xFFU, 0x00U,
    0xFFU
	};

  while(1)
  {	// Mientras el usuario no decidio finalizar secuencia
    for(int i=0; i<13; i++)
    {	// Recorro los 18 valores de mi LUT
    	(lut[i] & 0x01) ? digitalWrite(outLED[0], HIGH) : digitalWrite(outLED[0], LOW);
    	(lut[i] & 0x02) ? digitalWrite(outLED[1], HIGH) : digitalWrite(outLED[1], LOW);
  		(lut[i] & 0x04) ? digitalWrite(outLED[2], HIGH) : digitalWrite(outLED[2], LOW);
  		(lut[i] & 0x08) ? digitalWrite(outLED[3], HIGH) : digitalWrite(outLED[3], LOW);
  		(lut[i] & 0x10) ? digitalWrite(outLED[4], HIGH) : digitalWrite(outLED[4], LOW);
  		(lut[i] & 0x20) ? digitalWrite(outLED[5], HIGH) : digitalWrite(outLED[5], LOW);
  		(lut[i] & 0x40) ? digitalWrite(outLED[6], HIGH) : digitalWrite(outLED[6], LOW);
  		(lut[i] & 0x80) ? digitalWrite(outLED[7], HIGH) : digitalWrite(outLED[7], LOW);
      if(delayInter(6, ejecucion))
      {
        return;
      }
    }
  }
}

void barras(short int ejecucion)
{
  system("clear");
  printf("%s", titulo(8));

  for(int i=0; i<4; i++)
  {
    digitalWrite(outLED[i], HIGH);
    if(delayInter(7, ejecucion))
    {
      return;
    }
  }

  while(1)
  {
    for(int i=0; i<4; i++)
    {
      digitalWrite(outLED[3-i], LOW);
      digitalWrite(outLED[7-i], HIGH);

      if(delayInter(7, ejecucion))
      {
        return;
      }
    }

    for(int i=0; i<4; i++)
    {
      digitalWrite(outLED[i], HIGH);
      digitalWrite(outLED[4+i], LOW);

      if(delayInter(7, ejecucion))
      {
        return;
      }
    }
  }
}

//---------------------------------

//----------------------Menu-----------------------------
short int menu(short int ejecucion)
{
  char opt;
  char recib;
  short int flag = 0;

  if(ejecucion != 2)
  {
    system("clear");
    printf("\n\nSeleccione la secuencia de LEDs deseada\n");
    printf("\n1- El auto fantastico\n2- El choque\n3- La apilada\n4- La carrera\n5- Azar\n6- Anulacion\n7- Zigzag\n8- Barras\n9- SALIR\n\nOpcion: \n");
  }

  else
  {
    system("clear");
    printf("Modo esclavo\n\nPresione \"q\" para cerrar\n\n");
  }

  if((ejecucion == 0) || (ejecucion == 1))
    scanf("%c", &opt);

  else // Si vale 2
  {
    while(flag == 0)
    {
      if(serialDataAvail(puerto))
      {
        opt = serialGetchar(puerto);

        if((opt >= '1') && (opt <= '8'))
        {
          flag = 1;
        }

        serialFlush(puerto);
      }

      else if(kbhit())
      {
        opt = getchar();

        if((opt == 'q') || (opt == 'Q'))
        {
          return 1;
        }
      }
    }
  }

  switch(opt)
  {
    case '1':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      autoFantastico(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(1));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '2':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      choque(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(2));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '3':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      apilada(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(3));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '4':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      carrera(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(4));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '5':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      azar(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(5));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '6':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      anulacion(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(6));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '7':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      zigzag(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(7));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '8':
    system("clear");
    if((ejecucion==0)||(ejecucion==2))
    {
      barras(ejecucion);
    }
    else
    {
      serialPutchar(puerto, opt);
      system("clear");
      printf("%s", titulo(8));
      initTermios();
      while(1)
      {
        if(kbhit())
        {
          recib = getchar();

          if((recib=='q')||(recib=='Q'))
          {
            serialPutchar(puerto, recib);
            break;
          }

          else if(recib=='\033')
          {
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            recib = getchar();
            serialPutchar(puerto, recib);
            if(recib == 'A')
              system("echo VELOCIDAD +");
            else if(recib == 'B')
              system("echo VELOCIDAD -");
          }
        }
      }
      resetTermios();
    }
    break;

    case '9':
    return 1;
    break;

    default:
    system("clear");
    printf("\n\nOPCION INVALIDA\n");
    break;
  }

  return 0;
}

//-------------------------------------------------------


int main(void)
{ // INICIO FUNCION MAIN
  int i;
  int opt;
  char clave[] = "nacho";
  char *cadena;

  short int ejecucion, corte=0;

  pioInit();

  //--------------------Comunicacion Serial--------------------------------------------
  if((puerto = serialOpen("/dev/serial0", 115200)) < 0)			/* abrir el puerto serie */
  {
    system("clear");
    printf("\nERROR EN CONEXION SERIAL!\n\n");
    return 0;
  }

  //-----------------------------------------------------------------------------------

  system("clear");

  //-----INICIALIZA PINES--------

  for(int i=0;i<8;i++)
  	pinMode( outLED[i], OUTPUT);

  //------------------------------

  for(i=0; i<3; i++)
  {
    printf("Ingresar clave: ");
    cadena = getch();
    if(strcmp(clave, cadena) == 0)
    {
      system("clear");
      printf("\nBienvenido al sistema!\n\033[1;31mBy Toni & Bigote\033[0m\n");
      break;
    }

    else
    {
      printf("\nClave incorrecta!\n");
      if(i==2)
      {
        return 0;
      }
    }
  }

  do
  {
    printf("\n\nSeleccione el tipo de ejecucion\n");
    printf("\n1- Local\n2- Remota\n3- SALIR\n\nOpcion: ");
    scanf("%d", &opt);

    switch(opt)
    {
      case 1:
      ejecucion=0; // Local
      break;

      case 2: // Remota

      system("clear");
      do
      {
        printf("\n\nModo del dispositivo\n");
        printf("\n1- Maestro\n2- Esclavo\n3- SALIR\n\nOpcion: ");
        scanf("%d", &opt);

        switch(opt)
        {
          case 1:
          ejecucion=1; // Maestro
          break;

          case 2:
          ejecucion=2; // Esclavo
          break;

          case 3:
          system("clear");
          return 0; // Salir
          continue;

          default:
          system("clear");
          printf("\n\nOPCION INVALIDA\n");
          break;
        }
      }while((opt>3)||(opt<1));
      break;

      case 3:
      system("clear");
      return 0; // Salir
      continue;

      default:
      system("clear");
      printf("\n\nOPCION INVALIDA\n");
      break;
    }
  }while((opt>3)||(opt<1));

  while(corte == 0)
  {
    corte = menu(ejecucion);
  }

  serialClose(puerto);
  system("clear");
  return 0;
}
