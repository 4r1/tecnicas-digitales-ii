#include "EasyPIO.h"
#include <stdio.h>

int main () {
	pioInit();

	pinMode(23,OUTPUT); //led0
	pinMode(24,OUTPUT); //led1
	pinMode(25,OUTPUT); //led2
	pinMode(12,OUTPUT); //led3

	pinMode(17,INPUT); //sw1
	pinMode(5,INPUT); //dip0
	pinMode(6,INPUT); //dip1
	pinMode(13,INPUT); //dip2
	pinMode(19,INPUT); //dip3

	while( digitalRead(17) == 0 );

	digitalWrite( 23, digitalRead(5));
	digitalWrite( 24, digitalRead(6));
	digitalWrite( 25, digitalRead(13));
	digitalWrite( 12, digitalRead(19));

}
