.arch armv6
.global main

main:
	mov R1, #a
	bl while
	b end

while:
	cmp R1, #0
	beq finwhile
	sub R1, R1, #1
	b while
finwhile:
	mov pc, lr

a: .word 3122

end:
